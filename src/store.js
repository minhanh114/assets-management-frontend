import {applyMiddleware, combineReducers, createStore} from "redux";
import { authReducer } from "./reducers/user";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

const reducers = combineReducers({
    auth: authReducer
});

const middleware = [thunk]

const store = createStore(reducers, composeWithDevTools(applyMiddleware(...middleware)));

export default store;