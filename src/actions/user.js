import axios from "axios";

const userActions = {
    login: (username, password) => async (dispatch) => {
        try {
            dispatch({ type: "LOGIN_REQUEST" })

            const config = {
                headers: {
                    "Content-Type": "application/json"
                }
            };
            const { data } = await axios.post('api/v1/auth/login', { username, password }, config);
            dispatch({
                type: "LOGIN_SUCCESS",
                payload: data.user
            })

        } catch (error) {
            dispatch({
                type: "LOGIN_ERROR",
                payload: error.response.data.message
            })
        }
    },
    clearErrors: () => async (dispatch) => {
        dispatch({
            type: 'CLEAR_ERRORS'
        })
    }
}


export default userActions;

export const clearErrors = () => async (dispatch) => {
    dispatch({
        type: "CLEAR_ERRORS"
    })
}