import { Fragment } from "react";

const Portal = ({ history }) => {
    return (
        <Fragment>
            <div className="portal-container">
                <div className="portal-namecard">
                    <div className="col-20 col-lg-10">
                    </div>
                    <div className="green-box">
                    </div>
                </div>
                <div className="black-overlay">
                    <div className="color-block blue">
                        <header>
                            <h1>Header</h1>
                        </header>
                        <div className="content">
                            <h1>Content</h1>
                        </div>
                        <footer>
                            <h1>Footer</h1>
                        </footer>
                    </div>
                    <div className="color-block red">
                        <h1>huhu</h1>
                    </div>
                    <div className="color-block purple">
                        <h1>hehe</h1>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Portal;