import { Fragment, useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux";
import userActions, { clearErrors } from "../../actions/user";
import { useAlert } from "react-alert";
import { Link } from "react-router-dom";

const Login = ({ history }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const alert = useAlert()
    const dispatch = useDispatch();
    const { isAuthenticated, error } = useSelector(state => state.auth)

    useEffect(() => {
        if (isAuthenticated) {
            // history.push("") redirect
            alert.success("Login successfull !");
        }
        if (error) {
            alert.error(error);
            dispatch(clearErrors)
        }
    }, [dispatch, alert, isAuthenticated, error])

    const submitHandle = (e) => {
        e.preventDefault()
        dispatch(userActions.login(username, password))
    };

    return (
        <Fragment>
            <div className="row wrapper">
                <div className="col-10 col-lg-5">
                    <form className="shadow-lg" onSubmit={submitHandle}>
                        <h1 className="mb-3">Welcome to BeeTech !</h1>
                        <div className="form-group">
                            <label htmlFor="username_field">Username</label>
                            <br />
                            <input
                                type="username"
                                id="username_field"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                            ></input>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password_field">Password</label>
                            <br />
                            <input
                                type="password"
                                id="password_field"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            ></input>
                        </div>

                        <button
                            id="login_button"
                            type="submit"
                            className="btn btn-block py-3"
                        >
                            Login
                        </button>
                        <Link to="/register" className="float-right mt-3">
                            Register Now !
                        </Link>
                    </form>
                </div>
            </div>
        </Fragment>
    )
};

export default Login;